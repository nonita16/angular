import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import user
import { HomeComponent } from './components/home.component';
import { UserEditComponent } from './components/user-edit.component';

// import employee
import { EmployeeListComponent } from './components/employee-list.component';
import { EmployeeAddComponent } from './components/employee-add.component';
import { EmployeeEditComponent } from './components/employee-edit.component';
// import { EmployeeDetailComponent } from './components/employee-detail.component';

// import album
// import { AlbumAddComponent } from './components/album-add.component';
// import { AlbumEditComponent } from './components/album-edit.component';



const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'employee/:page', component: EmployeeListComponent},
    {path: 'add-employee', component: EmployeeAddComponent},
    {path: 'edit-employee/:id', component: EmployeeEditComponent},
    // {path: 'employeea/:id', component: EmployeeDetailComponent},
    // {path: 'crear-album/:employee', component: AlbumAddComponent},
    // {path: 'editar-album/:id', component: AlbumEditComponent},
    {path: 'mis-datos', component: UserEditComponent},
    {path: '**', component: HomeComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);



