import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { Employee } from '../models/employee'; 


@Injectable()
export class EmployeeService{
    public url: string;

    constructor(private _http: Http){
        this.url = GLOBAL.url;
    }

    getEmployees(page){
        let headers = new Headers({
            'Content-Type':'application/json',
            'Authorization':'7cdc36cc-e773-43ee-9916-8d3be4b0b929'
        });

        let options = new RequestOptions({headers: headers});
        return this._http.get(this.url+'employee/'+page+'/'+1000, options).map( res => res.json());

    }

    getEmployee(id: string){
        let headers = new Headers({
            'Content-Type':'application/json',
            // 'Authorization':token
        });

        let options = new RequestOptions({headers: headers});
        return this._http.get(this.url+'employee/'+id, options).map( res => res.json());

    }

    addEmployee(employee: Employee){
        let params = JSON.stringify(employee);
        let headers = new Headers({
            'Content-Type':'application/json',
            // 'Authorization':token
        });

        return this._http.post(this.url+'employee', params, {headers})
                         .map(res => res.json());
    }

    editEmployee(id: string, employee: Employee){
        let params = JSON.stringify(employee);
        let headers = new Headers({
            'Content-Type':'application/json',
            // 'Authorization':token
        });

        return this._http.put(this.url+'employee/', params, {headers})
                         .map(res => res.json());
    }

    deleteEmployee(token, id: string){
        let headers = new Headers({
            'Content-Type':'application/json',
            'Authorization':token
        });

        let options = new RequestOptions({headers: headers});
        return this._http.delete(this.url+'employee/'+id, options).map( res => res.json());

    }

} 